import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: const MyHomePage(title: 'First Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:Container(
        color: Colors.tealAccent[100],
        constraints:BoxConstraints.expand(),
        child: SingleChildScrollView(
            child: Column(
              children: [CircleAvatar(
              radius: 70,
              child: Icon(
                Icons.ac_unit,
                size: 100,
              ),
            ),


        Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("Row Child 1",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                  Text("Row Child 1",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),),
                  Text("Row Child 1",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40,top: 60,right: 40),
                child: Text("This is Column",
                  style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              //Text(),
              Padding(
                padding: const EdgeInsets.all(15),
                child: TextField(
                  decoration:InputDecoration(
                    hintText: 'Username',
                    fillColor: Colors.black12,
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
                child: TextField(
                  decoration:InputDecoration(
                    hintText: 'Pasword',
                    fillColor: Colors.black12,
                    filled: true,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                 ElevatedButton(
                     onPressed: (){},
                     child: Text('Cancle'),
                   style: ElevatedButton.styleFrom(
                     primary: Colors.black,
                     onPrimary: Colors.white,
                     padding:
                       EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                     textStyle: TextStyle(fontSize: 20),
                   ),
                 ),
                    ElevatedButton(
                      onPressed: (){},
                      child: Text('Login'),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.black,
                        onPrimary: Colors.white,
                        padding:
                        EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                        textStyle: TextStyle(fontSize: 20),
                      ),
                    ),

                ],


              ),

            ],
          ),
        ),
      ),
    );
  }
}
